# Thomas Reitelbach <tr@erdfunkstelle.de>, 2008, 2009, 2010.
# Burkhard Lück <lueck@hube-lueck.de>, 2008, 2009, 2019, 2020.
# Panagiotis Papadopoulos <pano_90@gmx.net>, 2010.
# Frederik Schwarzer <schwarzer@kde.org>, 2010, 2011, 2012, 2015, 2016.
# Alois Spitzbart <spitz234@hotmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcm_pci\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-24 00:45+0000\n"
"PO-Revision-Date: 2022-10-05 22:34+0200\n"
"Last-Translator: Alois Spitzbart <spitz234@hotmail.com>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Thomas Reitelbach"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "tr@erdfunkstelle.de"

#: main.cpp:26
#, kde-format
msgctxt "@label kcm name"
msgid "PCI"
msgstr "PCI"

#: main.cpp:27
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:14
msgctxt "@info:whatsthis"
msgid "PCI Information"
msgstr "Informationen zu PCI"

#~ msgid "kcm_pci"
#~ msgstr "kcm_pci"

#~ msgid "PCI Devices"
#~ msgstr "PCI-Geräte"

#~ msgid "(c) 2008 Nicolas Ternisien(c) 1998 - 2002 Helge Deller"
#~ msgstr ""
#~ "© 2008 Nicolas Ternisien\n"
#~ "© 1998–2002 Helge Deller"

#~ msgid "Nicolas Ternisien"
#~ msgstr "Nicolas Ternisien"

#~ msgid "Helge Deller"
#~ msgstr "Helge Deller"

#~ msgid "This list displays PCI information."
#~ msgstr "Diese Liste zeigt Informationen zum PCI-Bus an."

#~ msgid ""
#~ "This display shows information about your computer's PCI slots and the "
#~ "related connected devices."
#~ msgstr ""
#~ "Hier sehen Sie Informationen zu PCI-Steckplätzen und den zugehörigen PCI-"
#~ "Geräten in Ihrem Rechner."

#~ msgid "Device Class"
#~ msgstr "Geräteklasse"

#~ msgid "Device Subclass"
#~ msgstr "Geräte-Unterklasse"

#~ msgid "Device Programming Interface"
#~ msgstr "Geräte-Programmierschnittstelle"

#~ msgid "Master IDE Device"
#~ msgstr "Haupt-IDE-Gerät"

#~ msgid "Secondary programmable indicator"
#~ msgstr "Programmierbarer Schalter für „Sekundär“"

#~ msgid "Secondary operating mode"
#~ msgstr "Betriebsmodus „Sekundär“"

#~ msgid "Primary programmable indicator"
#~ msgstr "Programmierbarer Schalter für „Primär“"

#~ msgid "Primary operating mode"
#~ msgstr "Betriebsmodus „Primär“"

#~ msgid "Vendor"
#~ msgstr "Hersteller"

#~ msgid "Device"
#~ msgstr "Gerät"

#~ msgid "Subsystem"
#~ msgstr "Subsystem"

#~ msgid " - device:"
#~ msgstr " – Gerät:"

#~ msgid "Interrupt"
#~ msgstr "Interrupt"

#~ msgid "IRQ"
#~ msgstr "IRQ"

#~ msgid "Pin"
#~ msgstr "Pin"

#~ msgid "Control"
#~ msgstr "Kontrolle"

#~ msgid "Response in I/O space"
#~ msgstr "Antwort im E/A-Bereich"

#~ msgid "Response in memory space"
#~ msgstr "Antwort im Speicherbereich"

#~ msgid "Bus mastering"
#~ msgstr "Bus-Mastering"

#~ msgid "Response to special cycles"
#~ msgstr "Antwort auf spezielle Zyklen"

#~ msgid "Memory write and invalidate"
#~ msgstr "Speicher schreiben und ungültig machen"

#~ msgid "Palette snooping"
#~ msgstr "Palette-Snooping"

#~ msgid "Parity checking"
#~ msgstr "Paritätsprüfung"

#~ msgid "Address/data stepping"
#~ msgstr "Adress-/Datenschritte"

#~ msgid "System error"
#~ msgstr "Systemfehler"

#~ msgid "Back-to-back writes"
#~ msgstr "Back-to-back-Schreiben"

#~ msgid "Status"
#~ msgstr "Status"

#~ msgid "Interrupt status"
#~ msgstr "Interrupt-Status"

#~ msgid "Capability list"
#~ msgstr "Fähigkeiten-Liste"

#~ msgid "66 MHz PCI 2.1 bus"
#~ msgstr "66 MHz PCI 2.1 Bus"

#~ msgid "User-definable features"
#~ msgstr "Benutzerdefinierbare Funktionen"

#~ msgid "Accept fast back-to-back"
#~ msgstr "Schnelles back-to-back zulassen"

#~ msgid "Data parity error"
#~ msgstr "Datenparitätsfehler"

#~ msgid "Device selection timing"
#~ msgstr "Zeit für Geräteauswahl"

#~ msgid "Signaled target abort"
#~ msgstr "Ziel-Abbruch signalisieren"

#~ msgid "Received target abort"
#~ msgstr "Ziel-Abbruch empfangen"

#~ msgid "Received master abort"
#~ msgstr "Haupt-Abbruch empfangen"

#~ msgid "Signaled system error"
#~ msgstr "Systemfehler signalisiert"

#~ msgid "Parity error"
#~ msgstr "Paritätsfehler"

#~ msgid "Latency"
#~ msgstr "Latenz"

#~ msgid "MIN_GNT"
#~ msgstr "MIN_GNT"

#~ msgid "No major requirements (0x00)"
#~ msgstr "Keine größeren Voraussetzungen (0x00)"

#~ msgid "MAX_LAT"
#~ msgstr "MAX_LAT"

#~ msgid "Header"
#~ msgstr "Vorspann"

#~ msgid "Type"
#~ msgstr "Typ"

#~ msgid "Multifunctional"
#~ msgstr "Multifunktional"

#~ msgid "Build-in self test"
#~ msgstr "Eingebauter Selbsttest"

#~ msgid "BIST Capable"
#~ msgstr "BIST-Fähig"

#~ msgid "BIST Start"
#~ msgstr "BIST-Start"

#~ msgid "Completion code"
#~ msgstr "Vervollständigungs-Code"

#~ msgid "Size"
#~ msgstr "Größe"

#~ msgid "Address mappings"
#~ msgstr "Adress-Zuordnungen"

#~ msgid "Mapping %1"
#~ msgstr "Zuordnung von %1"

#~ msgid "Space"
#~ msgstr "Bereich"

#~ msgid "I/O"
#~ msgstr "E/A"

#~ msgid "Memory"
#~ msgstr "Speicher"

#~ msgid "Prefetchable"
#~ msgstr "Vorausladbar"

#~ msgid "Address"
#~ msgstr "Adresse"

#~ msgctxt "unassigned address"
#~ msgid "Unassigned"
#~ msgstr "Nicht zugewiesen"

#~ msgctxt "unassigned size"
#~ msgid "Unassigned"
#~ msgstr "Nicht zugewiesen"

#~ msgid "Bus"
#~ msgstr "Bus"

#~ msgid "Primary bus number"
#~ msgstr "Primäre Busnummer"

#~ msgid "Secondary bus number"
#~ msgstr "Sekundäre Busnummer"

#~ msgid "Subordinate bus number"
#~ msgstr "Untergeordnete Busnummer"

#~ msgid "Secondary latency timer"
#~ msgstr "Sekundärer Latenz-Zeitgeber"

#~ msgid "CardBus number"
#~ msgstr "CardBus-Nummer"

#~ msgid "CardBus latency timer"
#~ msgstr "CardBus-Latenz-Zeitgeber"

#~ msgid "Secondary status"
#~ msgstr "Sekundärer Status"

#~ msgid "I/O behind bridge"
#~ msgstr "E/A hinter Brücke"

#~ msgid "32-bit"
#~ msgstr "32-Bit"

#~ msgid "Base"
#~ msgstr "Basis"

#~ msgid "Limit"
#~ msgstr "Beschränkung"

#~ msgid "Memory behind bridge"
#~ msgstr "Speicher hinter Brücke"

#~ msgid "Prefetchable memory behind bridge"
#~ msgstr "Vorausladbarer Speicher hinter Brücke"

#~ msgid "64-bit"
#~ msgstr "64-Bit"

#~ msgid "Bridge control"
#~ msgstr "Steuerung der Brücke"

#~ msgid "Secondary parity checking"
#~ msgstr "Paritätsprüfung des Sekundärgeräts"

#~ msgid "Secondary system error"
#~ msgstr "Systemfehler des Sekundärgeräts"

#~ msgid "ISA ports forwarding"
#~ msgstr "ISA-Port-Weiterleitung"

#~ msgid "VGA forwarding"
#~ msgstr "VGA-Weiterleitung"

#~ msgid "Master abort"
#~ msgstr "Haupt-Abbruch"

#~ msgid "Secondary bus reset"
#~ msgstr "Sekundär-Bus-Zurücksetzung"

#~ msgid "Secondary back-to-back writes"
#~ msgstr "back-to-back-Schreiben für Sekundär-Bus"

#~ msgid "Primary discard timer counts"
#~ msgstr "Anzahl Verzögerungs-Takte des primären Gerätes"

#~ msgid "2e10 PCI clocks"
#~ msgstr "2e10 PCI-Zeitgeber"

#~ msgid "2e15 PCI clocks"
#~ msgstr "2e15 PCI-Zeitgeber"

#~ msgid "Secondary discard timer counts"
#~ msgstr "Anzahl Verzögerungs-Takte des sekundären Gerätes"

#~ msgid "Discard timer error"
#~ msgstr "Verzögerungs-Takt-Fehler"

#~ msgid "Discard timer system error"
#~ msgstr "Verzögerungs-Takt-Systemfehler"

#~ msgid "Expansion ROM"
#~ msgstr "Erweiterungs-ROM"

#~ msgid "Memory windows"
#~ msgstr "Speicher-Fenster"

#~ msgid "Window %1"
#~ msgstr "Fenster %1"

#~ msgid "I/O windows"
#~ msgstr "E/A-Fenster"

#~ msgid "16-bit"
#~ msgstr "16-Bit"

#~ msgid "16-bit legacy interface ports"
#~ msgstr "16-Bit Legacy-Ports"

#~ msgid "CardBus control"
#~ msgstr "CardBus-Steuerung"

#~ msgid "Interrupts for 16-bit cards"
#~ msgstr "Interrupts für 16-Bit-Karten"

#~ msgid "Window 0 prefetchable memory"
#~ msgstr "Fenster 0 – Vorausladbarer Speicher"

#~ msgid "Window 1 prefetchable memory"
#~ msgstr "Fenster 1 – Vorausladbarer Speicher"

#~ msgid "Post writes"
#~ msgstr "Zeitversetztes Schreiben"

#~ msgid "Raw PCI config space"
#~ msgstr "Roher PCI-Einrichtungsspeicher"

#~ msgid "Capabilities"
#~ msgstr "Fähigkeiten"

#~ msgid "Version"
#~ msgstr "Version"

#~ msgid "Clock required for PME generation"
#~ msgstr "Zeitgeber benötigt für PME-Erzeugung"

#~ msgid "Device-specific initialization required"
#~ msgstr "Gerätespezifische Initialisierung notwendig"

#~ msgid "Maximum auxiliary current required in D3 cold"
#~ msgstr "Maximale notwendige externe Spannung in „D3 kalt“"

#~ msgid "D1 support"
#~ msgstr "D1-Unterstützung"

#~ msgid "D2 support"
#~ msgstr "D2-Unterstützung"

#~ msgid "Power management events"
#~ msgstr "Ereignisse der Energieverwaltung"

#~ msgid "D0"
#~ msgstr "D0"

#~ msgid "D1"
#~ msgstr "D1"

#~ msgid "D2"
#~ msgstr "D2"

#~ msgid "D3 hot"
#~ msgstr "D3 heiß"

#~ msgid "D3 cold"
#~ msgstr "D3 kalt"

#~ msgid "Power state"
#~ msgstr "Energiestatus"

#~ msgid "Power management"
#~ msgstr "Energieverwaltung"

#~ msgid "Data select"
#~ msgstr "Datenauswahl"

#~ msgid "Data scale"
#~ msgstr "Datenskalierung"

#~ msgid "Power management status"
#~ msgstr "Status der Energieverwaltung"

#~ msgid "Bridge status"
#~ msgstr "Brücken-Status"

#~ msgid "Secondary bus state in D3 hot"
#~ msgstr "Status des sekundären Bus in D3 heiß"

#~ msgid "B2"
#~ msgstr "B2"

#~ msgid "B3"
#~ msgstr "B3"

#~ msgid "Secondary bus power & clock control"
#~ msgstr "Energie- und Zeitgeberkontrolle des sekundären Bus"

#~ msgid "Data"
#~ msgstr "Daten"

#~ msgid "Revision"
#~ msgstr "Revision"

#~ msgid "Rate"
#~ msgstr "Rate"

#~ msgid "AGP 3.0 mode"
#~ msgstr "AGP-3.0-Modus"

#~ msgid "Fast Writes"
#~ msgstr "Schnelles Schreiben"

#~ msgid "Address over 4 GiB"
#~ msgstr "Adressierung über 4 GiB"

#~ msgid "Translation of host processor access"
#~ msgstr "Übersetzung für Host-Prozessor-Zugriff"

#~ msgid "64-bit GART"
#~ msgstr "64-Bit-GART"

#~ msgid "Cache Coherency"
#~ msgstr "Zwischenspeicher-Zusammenhang"

#~ msgid "Side-band addressing"
#~ msgstr "Seitenband-Adressierung"

#~ msgid "Calibrating cycle"
#~ msgstr "Kalibierungs-Zyklus"

#~ msgid "Optimum asynchronous request size"
#~ msgstr "Optimale Größe für asynchrone Anfragen"

#~ msgid "Isochronous transactions"
#~ msgstr "Zeitgleiche Transaktionen"

#~ msgid "Maximum number of AGP command"
#~ msgstr "Maximale Anzahl AGP-Befehle"

#~ msgid "Configuration"
#~ msgstr "Einrichtung"

#~ msgid "AGP"
#~ msgstr "AGP"

#~ msgid "Data address"
#~ msgstr "Datenadresse"

#~ msgid "Transfer completed"
#~ msgstr "Übertragung abgeschlossen"

#~ msgid "Message control"
#~ msgstr "Meldungs-Kontrolle"

#~ msgid "Message signaled interrupts"
#~ msgstr "Meldungs-signalisierte Unterbrechungsanforderungen"

#~ msgid "Multiple message capable"
#~ msgstr "Kann mit mehreren Nachrichten umgehen"

#~ msgid "Multiple message enable"
#~ msgstr "„Mehrere Nachrichten“ aktivieren"

#~ msgid "64-bit address"
#~ msgstr "64-Bit-Adresse"

#~ msgid "Per vector masking"
#~ msgstr "Maskierung pro Vektor"

#~ msgid "Mask"
#~ msgstr "Maskierung"

#~ msgid "Pending"
#~ msgstr "Ausstehend"

#~ msgid "Length"
#~ msgstr "Länge"

#~ msgctxt "no data"
#~ msgid "None"
#~ msgstr "Keine"

#~ msgid "Next"
#~ msgstr "Nächste"

#~ msgid "0x00 (None)"
#~ msgstr "0x00 (Keine)"

#~ msgid "Root only"
#~ msgstr "Nur Systemverwalter"

#~ msgid "Value"
#~ msgstr "Wert"

#~ msgid "Cache line size"
#~ msgstr "Zwischenspeicher-Zeilengröße"

#~ msgctxt "state of PCI item"
#~ msgid "Enabled"
#~ msgstr "Aktiviert"

#~ msgctxt "state of PCI item"
#~ msgid "Disabled"
#~ msgstr "Deaktiviert"

#~ msgctxt "state of PCI item"
#~ msgid "Yes"
#~ msgstr "Ja"

#~ msgctxt "state of PCI item"
#~ msgid "No"
#~ msgstr "Nein"

#~ msgctxt "state of PCI item"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgid "Unclassified device"
#~ msgstr "Unklassifiziertes Gerät"

#~ msgid "Mass storage controller"
#~ msgstr "Massenspeicher-Controller"

#~ msgid "Network controller"
#~ msgstr "Netzwerk-Controller"

#~ msgid "Display controller"
#~ msgstr "Grafik-Controller"

#~ msgid "Multimedia controller"
#~ msgstr "Multimedia-Controller"

#~ msgid "Memory controller"
#~ msgstr "Speicher-Controller"

#~ msgid "Bridge"
#~ msgstr "Brücke"

#~ msgid "Communication controller"
#~ msgstr "Kommunikations-Controller"

#~ msgid "Generic system peripheral"
#~ msgstr "Allgemeines Peripheriegerät"

#~ msgid "Input device controller"
#~ msgstr "Eingabegerät-Controller"

#~ msgid "Docking station"
#~ msgstr "Andockstation"

#~ msgid "Processor"
#~ msgstr "Prozessor"

#~ msgid "Serial bus controller"
#~ msgstr "Controller für den seriellen Bus"

#~ msgid "Wireless controller"
#~ msgstr "Drahtlos-Controller"

#~ msgid "Intelligent controller"
#~ msgstr "Intelligenter Controller"

#~ msgid "Satellite communications controller"
#~ msgstr "Satelliten-Controller"

#~ msgid "Encryption controller"
#~ msgstr "Verschlüsselungs-Controller"

#~ msgid "Signal processing controller"
#~ msgstr "Signalverarbeitungs-Controller"

#~ msgid "Coprocessor"
#~ msgstr "Co-Prozessor"

#~ msgid "Unassigned class"
#~ msgstr "Nicht zugewiesene Klasse"

#~ msgid "Unknown device class"
#~ msgstr "Unbekannte Geräteklasse"

#~ msgid "Non-VGA unclassified device"
#~ msgstr "Unklassifiziertes Nicht-VGA-Gerät"

#~ msgid "VGA unclassified device"
#~ msgstr "Unklassifiziertes VGA-Gerät"

#~ msgid "Unknown unclassified device"
#~ msgstr "Unbekanntes unklassifiziertes Gerät"

#~ msgid "SCSI storage controller"
#~ msgstr "SCSI-Controller für Massenspeicher"

#~ msgid "IDE controller"
#~ msgstr "IDE-Controller"

#~ msgid "Floppy disk controller"
#~ msgstr "Diskettenlaufwerks-Controller"

#~ msgid "IPI bus controller"
#~ msgstr "IPI-Bus-Controller"

#~ msgid "RAID bus controller"
#~ msgstr "RAID-Controller"

#~ msgid "ATA controller"
#~ msgstr "ATA-Controller"

#~ msgid "SATA controller"
#~ msgstr "SATA-Controller"

#, fuzzy
#~| msgid "Serial controller"
#~ msgid "Serial Attached SCSI controller"
#~ msgstr "Seriell-Controller"

#, fuzzy
#~| msgid "Unknown memory controller"
#~ msgid "Non-Volatile memory controller"
#~ msgstr "Unbekannter Speicher-Controller"

#~ msgid "Unknown storage controller"
#~ msgstr "Unbekannter Massenspeicher-Controller"

#~ msgid "Ethernet controller"
#~ msgstr "Ethernet-Controller"

#~ msgid "Token ring network controller"
#~ msgstr "Token Ring Netzwerk-Controller"

#~ msgid "FDDI network controller"
#~ msgstr "FDDI-Netzwerk-Controller"

#~ msgid "ATM network controller"
#~ msgstr "ATM-Netzwerk-Controller"

#~ msgid "ISDN controller"
#~ msgstr "ISDN-Controller"

#~ msgid "WorldFip controller"
#~ msgstr "WorldFip-Controller"

#~ msgid "PICMG controller"
#~ msgstr "PICMG-Controller"

#~ msgid "Infiniband controller"
#~ msgstr "InfiniBand-Controller"

#, fuzzy
#~| msgid "RF controller"
#~ msgid "Fabric controller"
#~ msgstr "RF-Controller"

#~ msgid "Unknown network controller"
#~ msgstr "Unbekannter Netzwerk-Controller"

#~ msgid "VGA compatible controller"
#~ msgstr "VGA-kompatibler Controller"

#~ msgid "XGA compatible controller"
#~ msgstr "XGA-kompatibler Controller"

#~ msgid "3D controller"
#~ msgstr "3D-Controller"

#~ msgid "Unknown display controller"
#~ msgstr "Unbekannter Grafik-Controller"

#~ msgid "Multimedia video controller"
#~ msgstr "Multimedia-Video-Controller"

#~ msgid "Multimedia audio controller"
#~ msgstr "Multimedia-Audio-Controller"

#~ msgid "Computer telephony device"
#~ msgstr "Telefonie-Gerät"

#~ msgid "Audio device"
#~ msgstr "Audiogerät"

#~ msgid "Unknown multimedia controller"
#~ msgstr "Unbekannter Multimedia-Controller"

#~ msgid "RAM memory"
#~ msgstr "RAM-Speicher"

#~ msgid "FLASH memory"
#~ msgstr "FLASH-Speicher"

#~ msgid "Unknown memory controller"
#~ msgstr "Unbekannter Speicher-Controller"

#~ msgid "Host bridge"
#~ msgstr "Host-Brücke"

#~ msgid "ISA bridge"
#~ msgstr "ISA-Brücke"

#~ msgid "EISA bridge"
#~ msgstr "EISA-Brücke"

#~ msgid "MicroChannel bridge"
#~ msgstr "MicroChannel-Brücke"

#~ msgid "PCI bridge"
#~ msgstr "PCI-Brücke"

#~ msgid "PCMCIA bridge"
#~ msgstr "PCMCIA-Brücke"

#~ msgid "NuBus bridge"
#~ msgstr "NuBus-Brücke"

#~ msgid "CardBus bridge"
#~ msgstr "CardBus-Brücke"

#~ msgid "RACEway bridge"
#~ msgstr "RACEway-Brücke"

#~ msgid "Semi-transparent PCI-to-PCI bridge"
#~ msgstr "Semi-transparente PCI-zu-PCI-Brücke"

#~ msgid "InfiniBand to PCI host bridge"
#~ msgstr "InfiniBand-zu-PCI-Host-Brücke"

#~ msgid "Unknown bridge"
#~ msgstr "Unbekannte Brücke"

#~ msgid "Serial controller"
#~ msgstr "Seriell-Controller"

#~ msgid "Parallel controller"
#~ msgstr "Parallel-Controller"

#~ msgid "Multiport serial controller"
#~ msgstr "Mehrfachport-Seriell-Controller"

#~ msgid "Modem"
#~ msgstr "Modem"

#~ msgid "GPIB controller"
#~ msgstr "GPIB-Controller"

#~ msgid "Smart card controller"
#~ msgstr "Chipkarten-Controller"

#~ msgid "Unknown communication controller"
#~ msgstr "Unbekannter Kommunikations-Controller"

#~ msgid "PIC"
#~ msgstr "PIC"

#~ msgid "DMA controller"
#~ msgstr "DMA-Controller"

#~ msgid "Timer"
#~ msgstr "Zeitgeber"

#~ msgid "RTC"
#~ msgstr "RTC"

#~ msgid "PCI Hot-plug controller"
#~ msgstr "PCI-Hotplug-Controller"

#, fuzzy
#~| msgid "ISDN controller"
#~ msgid "SD Host controller"
#~ msgstr "ISDN-Controller"

#~ msgid "IOMMU"
#~ msgstr "IOMMU"

#~ msgid "Unknown system peripheral"
#~ msgstr "Unbekanntes Systemgerät"

#~ msgid "Keyboard controller"
#~ msgstr "Tastatur-Controller"

#~ msgid "Digitizer Pen"
#~ msgstr "Digitizer-Stift"

#~ msgid "Mouse controller"
#~ msgstr "Maus-Controller"

#~ msgid "Scanner controller"
#~ msgstr "Scanner-Controller"

#~ msgid "Gameport controller"
#~ msgstr "Gameport-Controller"

#~ msgid "Unknown input device controller"
#~ msgstr "Unbekannter Eingabegerät-Controller"

#~ msgid "Generic docking station"
#~ msgstr "Generische Andock-Station"

#~ msgid "Unknown docking station"
#~ msgstr "Unbekannte Andock-Station"

#~ msgid "386"
#~ msgstr "386"

#~ msgid "486"
#~ msgstr "486"

#~ msgid "Pentium"
#~ msgstr "Pentium"

#~ msgid "Alpha"
#~ msgstr "Alpha"

#~ msgid "Power PC"
#~ msgstr "Power PC"

#~ msgid "MIPS"
#~ msgstr "MIPS"

#~ msgid "Co-processor"
#~ msgstr "Co-Prozessor"

#~ msgid "Unknown processor"
#~ msgstr "Unbekannter Prozessor"

#~ msgid "FireWire (IEEE 1394)"
#~ msgstr "FireWire (IEEE 1394)"

#~ msgid "ACCESS bus"
#~ msgstr "ACCESS-Bus"

#~ msgid "SSA"
#~ msgstr "SSA"

#~ msgid "USB controller"
#~ msgstr "USB-Controller"

#~ msgid "Fibre channel"
#~ msgstr "Fibrechannel"

#~ msgid "SMBus"
#~ msgstr "SMBus"

#~ msgid "InfiniBand"
#~ msgstr "InfiniBand"

#~ msgid "IPMI interface"
#~ msgstr "IPMI-Schnittstelle"

#~ msgid "SERCOS interface"
#~ msgstr "SERCOS-Schnittstelle"

#~ msgid "CANbus"
#~ msgstr "CANbus"

#~ msgid "Unknown serial bus controller"
#~ msgstr "Unbekannter Seriell-Controller"

#~ msgid "IRDA controller"
#~ msgstr "IRDA-Controller"

#~ msgid "Consumer IR controller"
#~ msgstr "Consumer-IR-Controller"

#~ msgid "RF controller"
#~ msgstr "RF-Controller"

#~ msgid "Bluetooth"
#~ msgstr "Bluetooth"

#~ msgid "Broadband"
#~ msgstr "Breitband"

#~ msgid "Ethernet (802.11a - 5 GHz)"
#~ msgstr "Ethernet (802.11a – 5 GHz)"

#~ msgid "Ethernet (802.11b - 2.4 GHz)"
#~ msgstr "Ethernet (802.11b – 2.4 GHz)"

#~ msgid "Unknown wireless controller"
#~ msgstr "Unbekannter Drahtlos-Controller"

#~ msgid "I2O"
#~ msgstr "I2O"

#~ msgid "Unknown intelligent controller"
#~ msgstr "Unbekannter intelligenter Controller"

#~ msgid "Satellite TV controller"
#~ msgstr "Satelliten-TV-Controller"

#~ msgid "Satellite audio communication controller"
#~ msgstr "Satelliten-Audio-Kommunikations-Controller"

#~ msgid "Satellite voice communication controller"
#~ msgstr "Satelliten-Stimmen-Kommunikations-Controller"

#~ msgid "Satellite data communication controller"
#~ msgstr "Satelliten-Daten-Kommunikations-Controller"

#~ msgid "Unknown satellite communications controller"
#~ msgstr "Unbekannter Satelliten-Kommunikations-Controller"

#~ msgid "Network and computing encryption device"
#~ msgstr "Verschlüsselungs-Controller für Netzwerk und Rechner"

#~ msgid "Entertainment encryption device"
#~ msgstr "Verschlüsselungs-Controller für Unterhaltung"

#~ msgid "Unknown encryption controller"
#~ msgstr "Unbekannter Verschlüsselungs-Controller"

#~ msgid "DPIO module"
#~ msgstr "DPIO-Modul"

#~ msgid "Performance counters"
#~ msgstr "Geschwindigkeitszähler"

#~ msgid "Communication synchronizer"
#~ msgstr "Kommunikationsabgleich"

#~ msgid "Signal processing management"
#~ msgstr "Signalverarbeitungs-Verwaltung"

#~ msgid "Unknown signal processing controller"
#~ msgstr "Unbekannte Signalverarbeitungssteuerung"

#, fuzzy
#~| msgid "Unknown processor"
#~ msgid "Unknown processing accelerator"
#~ msgstr "Unbekannter Prozessor"

#~ msgid "Unknown subdevice class"
#~ msgstr "Unbekannte Untergeräteklasse"

#, fuzzy
#~| msgid "VGA compatible controller"
#~ msgid "ISA Compatibility mode-only controller"
#~ msgstr "VGA-kompatibler Controller"

#, fuzzy
#~| msgid "PCI Hot-plug controller"
#~ msgid "PCI native mode-only controller"
#~ msgstr "PCI-Hotplug-Controller"

#~ msgid "Vendor specific"
#~ msgstr "Anbieterspezifisch"

#~ msgid "AHCI 1.0"
#~ msgstr "AHCI 1.0"

#~ msgid "Serial Storage Bus"
#~ msgstr "Serieller Speicherbus"

#~ msgid "NVMHCI"
#~ msgstr "NVMHCI"

#~ msgid "NVM Express"
#~ msgstr "NVM Express"

#~ msgid "VGA controller"
#~ msgstr "VGA-Controller"

#~ msgid "8514 controller"
#~ msgstr "8514-Controller"

#~ msgid "Normal decode"
#~ msgstr "Normales Dekodieren"

#~ msgid "Subtractive decode"
#~ msgstr "Abziehendes Dekodieren"

#~ msgid "Transparent mode"
#~ msgstr "Transparenter Modus"

#~ msgid "Endpoint mode"
#~ msgstr "Endpunkt-Modus"

#~ msgid "Primary bus towards host CPU"
#~ msgstr "Primärer Bus durch Host-CPU"

#~ msgid "Secondary bus towards host CPU"
#~ msgstr "Primärer Bus durch Host-CPU"

#~ msgid "8250"
#~ msgstr "8250"

#~ msgid "16450"
#~ msgstr "16450"

#~ msgid "16550"
#~ msgstr "16550"

#~ msgid "16650"
#~ msgstr "16650"

#~ msgid "16750"
#~ msgstr "16750"

#~ msgid "16850"
#~ msgstr "16850"

#~ msgid "16950"
#~ msgstr "16950"

#~ msgid "SPP"
#~ msgstr "SPP"

#~ msgid "BiDir"
#~ msgstr "BiDir"

#~ msgid "ECP"
#~ msgstr "ECP"

#~ msgid "IEEE1284"
#~ msgstr "IEEE1284"

#~ msgid "IEEE1284 Target"
#~ msgstr "IEEE1284-Ziel"

#~ msgid "Generic"
#~ msgstr "Generisch"

#~ msgid "Hayes/16450"
#~ msgstr "Hayes/16450"

#~ msgid "Hayes/16550"
#~ msgstr "Hayes/16550"

#~ msgid "Hayes/16650"
#~ msgstr "Hayes/16650"

#~ msgid "Hayes/16750"
#~ msgstr "Hayes/16750"

#~ msgid "8259"
#~ msgstr "8259"

#~ msgid "ISA PIC"
#~ msgstr "ISA PIC"

#~ msgid "EISA PIC"
#~ msgstr "EISA PIC"

#~ msgid "IO-APIC"
#~ msgstr "IO-APIC"

#~ msgid "IO(X)-APIC"
#~ msgstr "IO(X)-APIC"

#~ msgid "8237"
#~ msgstr "8237"

#~ msgid "ISA DMA"
#~ msgstr "ISA DMA"

#~ msgid "EISA DMA"
#~ msgstr "EISA DMA"

#~ msgid "8254"
#~ msgstr "8254"

#~ msgid "ISA timer"
#~ msgstr "ISA-Zeitgeber"

#~ msgid "EISA timers"
#~ msgstr "EISA-Zeitgeber"

#~ msgid "HPET"
#~ msgstr "HPET"

#~ msgid "ISA RTC"
#~ msgstr "ISA RTC"

#~ msgid "Extended"
#~ msgstr "Erweitert"

#~ msgid "OHCI"
#~ msgstr "OHCI"

#~ msgid "UHCI"
#~ msgstr "UHCI"

#~ msgid "EHCI"
#~ msgstr "EHCI"

#~ msgid "XHCI"
#~ msgstr "XHCI"

#~ msgid "Unspecified"
#~ msgstr "Nicht spezifiziert"

#~ msgid "USB Device"
#~ msgstr "USB-Gerät"

#~ msgid "SMIC"
#~ msgstr "SMIC"

#~ msgid "Keyboard controller style"
#~ msgstr "Tastatur-Controller-Stil"

#~ msgid "Block transfer"
#~ msgstr "Blockübertragung"

#~ msgid "Vital product data"
#~ msgstr "Vital-Produkt-Daten"

#~ msgid "Slot identification"
#~ msgstr "Slot-Kennung"

#~ msgid "CompactPCI hot swap"
#~ msgstr "CompactPCI hot swap"

#~ msgid "HyperTransport"
#~ msgstr "HyperTransport"

#~ msgid "Debug port"
#~ msgstr "Debug-Port"

#~ msgid "CompactPCI central resource control"
#~ msgstr "CompactPCI zentrale Ressourcen-Kontrolle"

#~ msgid "PCI hot-plug"
#~ msgstr "PCI hot-plug"

#~ msgid "AGP x8"
#~ msgstr "AGP x8"

#~ msgid "Secure device"
#~ msgstr "Sicheres Gerät"

#~ msgid "PCI express"
#~ msgstr "PCI express"

#~ msgid "MSI-X"
#~ msgstr "MSI-X"

#~ msgid "PCI Advanced Features"
#~ msgstr "Erweiterte PCI-Funktionen"

#~ msgid "Fast"
#~ msgstr "Schnell"

#~ msgid "Medium"
#~ msgstr "Mittel"

#~ msgid "Slow"
#~ msgstr "Langsam"

#~ msgid "32 bit"
#~ msgstr "32 bit"

#~ msgid "Below 1M"
#~ msgstr "Unter 1M"

#~ msgid "64 bit"
#~ msgstr "64 bit"

#~ msgid "Standard"
#~ msgstr "Standard"

#~ msgid "CardBus"
#~ msgstr "CardBus"

#~ msgid "1X"
#~ msgstr "1X"

#~ msgid "2X"
#~ msgstr "2X"

#~ msgid "1X & 2X"
#~ msgstr "1X & 2X"

#~ msgid "4X"
#~ msgstr "4X"

#~ msgid "1X & 4X"
#~ msgstr "1X & 4X"

#~ msgid "2X & 4X"
#~ msgstr "2X & 4X"

#~ msgid "1X & 2X & 4X"
#~ msgstr "1X & 2X & 4X"

#~ msgid "8X"
#~ msgstr "8X"

#~ msgid "4X & 8X"
#~ msgstr "4X & 8X"

#~ msgid "4 ms"
#~ msgstr "4 ms"

#~ msgid "16 ms"
#~ msgstr "16 ms"

#~ msgid "64 ms"
#~ msgstr "64 ms"

#~ msgid "256 ms"
#~ msgstr "256 ms"

#~ msgid "Not needed"
#~ msgstr "Nicht benötigt"

#~ msgid "0 (self powered)"
#~ msgstr "0 (eigene Stromversorgung)"

#~ msgid "55 mA"
#~ msgstr "55 mA"

#~ msgid "100 mA"
#~ msgstr "100 mA"

#~ msgid "160 mA"
#~ msgstr "160 mA"

#~ msgid "220 mA"
#~ msgstr "220 mA"

#~ msgid "270 mA"
#~ msgstr "270 mA"

#~ msgid "320 mA"
#~ msgstr "320 mA"

#~ msgid "375 mA"
#~ msgstr "375 mA"

#~ msgid "1 vector"
#~ msgstr "1 Vektor"

#~ msgid "2 vectors"
#~ msgstr "2 Vektoren"

#~ msgid "4 vectors"
#~ msgstr "4 Vektoren"

#~ msgid "8 vectors"
#~ msgstr "8 Vektoren"

#~ msgid "16 vectors"
#~ msgstr "16 Vektoren"

#~ msgid "32 vectors"
#~ msgstr "32 Vektoren"

#~ msgid "Serial ATA direct port access"
#~ msgstr "Zugriff auf Serial-ATA Direct-Port"

#~ msgid "PICMG 2.14 multi computing"
#~ msgstr "PICMG 2.14 multi computing"

#~ msgid "GPIB (IEEE 488.1/2) controller"
#~ msgstr "GPIB (IEEE 488.1/2)-Controller"

#~ msgid "Smart card"
#~ msgstr "Smartcard"

#~ msgid "System peripheral"
#~ msgstr "Systemgerät"

#~ msgid "Management card"
#~ msgstr "Verwaltungskarte"

#~ msgid "single DMA"
#~ msgstr "DMA (einfach)"

#~ msgid "chained DMA"
#~ msgstr "DMA (verkettet)"

#~ msgid "VGA compatible"
#~ msgstr "VGA-kompatibel"

#~ msgid "8514 compatible"
#~ msgstr "8514-kompatibel"

#~ msgid "KDE PCI Information Control Module"
#~ msgstr "Kontrollmodul für PCI-Informationen"
