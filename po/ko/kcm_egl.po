# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kinfocenter package.
# Shinjo Park <kde@peremen.name>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: kinfocenter\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-17 00:29+0000\n"
"PO-Revision-Date: 2021-11-12 00:05+0100\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "박신조"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde@peremen.name"

#: main.cpp:23
#, kde-format
msgctxt "@label kcm name"
msgid "OpenGL (EGL)"
msgstr "OpenGL(EGL)"

#: main.cpp:24
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:14
msgctxt "@info:whatsthis"
msgid "OpenGL (EGL)"
msgstr "OpenGL(EGL)"
