# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kcm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-19 00:47+0000\n"
"PO-Revision-Date: 2014-10-20 20:42+0000\n"
"Last-Translator: Samir Ribić <Unknown>\n"
"Language-Team: bosanski <bs@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Launchpad (build 17341)\n"
"X-Launchpad-Export-Date: 2015-02-17 06:22+0000\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Lejla Agic,Samir Ribić"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Lagic1@etf.unsa.ba,samir.ribic@etf.unsa.ba"

#: CPUEntry.cpp:17
#, kde-format
msgid "Processor:"
msgid_plural "Processors:"
msgstr[0] "Procesor:"
msgstr[1] "Procesori:"
msgstr[2] "Procesori:"

#: CPUEntry.cpp:22
#, fuzzy, kde-format
#| msgctxt "Unknown amount of RAM"
#| msgid "Unknown"
msgctxt ""
"unknown CPU type/product name; presented as `Processors: 4 × Unknown Type'"
msgid "Unknown Type"
msgstr "Nepoznat"

#: GPUEntry.cpp:19
#, fuzzy, kde-format
#| msgid "Processor:"
#| msgid_plural "Processors:"
msgid "Graphics Processor:"
msgstr "Procesor:"

#: GraphicsPlatformEntry.cpp:10
#, fuzzy, kde-format
#| msgid "Processor:"
#| msgid_plural "Processors:"
msgid "Graphics Platform:"
msgstr "Procesor:"

#: KernelEntry.cpp:11
#, kde-format
msgid "Kernel Version:"
msgstr "Kernel Verzija:"

#: KernelEntry.cpp:23
#, kde-format
msgctxt "@label %1 is the kernel version, %2 CPU bit width (e.g. 32 or 64)"
msgid "%1 (%2-bit)"
msgstr ""

#: main.cpp:104
#, kde-format
msgctxt "@title"
msgid "About this System"
msgstr ""

#: main.cpp:107
#, fuzzy, kde-format
msgctxt "@info:credit"
msgid "Copyright 2012-2020 Harald Sitter"
msgstr "Autorska prava 2012-2014 Harald Sitter"

#: main.cpp:108
#, fuzzy, kde-format
msgctxt "@info:credit"
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: main.cpp:108
#, fuzzy, kde-format
msgctxt "@info:credit"
msgid "Author"
msgstr "Autor"

#: main.cpp:177
#, kde-format
msgctxt "@label"
msgid "Manufacturer:"
msgstr ""

#: main.cpp:180 main.cpp:197
#, kde-format
msgctxt "@label"
msgid "Product Name:"
msgstr ""

#: main.cpp:183
#, fuzzy, kde-format
#| msgid "Qt Version:"
msgctxt "@label"
msgid "System Version:"
msgstr "Qt Verzija:"

#: main.cpp:186 main.cpp:200
#, kde-format
msgctxt "@label"
msgid "Serial Number:"
msgstr ""

#: main.cpp:190 main.cpp:207
#, fuzzy, kde-format
#| msgctxt "Unknown amount of RAM"
#| msgid "Unknown"
msgctxt "@label unknown entry in table"
msgid "Unknown:"
msgstr "Nepoznat"

#: main.cpp:203
#, fuzzy, kde-format
#| msgid "Qt Version:"
msgctxt "@label uboot is the name of a bootloader for embedded devices"
msgid "U-Boot Version:"
msgstr "Qt Verzija:"

#: main.cpp:241
#, fuzzy, kde-format
msgid "KDE Frameworks Version:"
msgstr "KDELibs Verzija:"

#: main.cpp:242
#, kde-format
msgid "Qt Version:"
msgstr "Qt Verzija:"

#: MemoryEntry.cpp:20
#, kde-format
msgid "Memory:"
msgstr "Memorija:"

#: MemoryEntry.cpp:49
#, kde-format
msgctxt "@label %1 is the formatted amount of system memory (e.g. 7,7 GiB)"
msgid "%1 of RAM"
msgstr "%1 RAM-a"

#: MemoryEntry.cpp:53
#, kde-format
msgctxt "Unknown amount of RAM"
msgid "Unknown"
msgstr "Nepoznat"

#: OSVersionEntry.cpp:9
#, kde-format
msgid "Operating System:"
msgstr ""

#: OSVersionEntry.cpp:11
#, kde-format
msgctxt "@label %1 is the distro name, %2 is the version"
msgid "%1 %2"
msgstr ""

#: OSVersionEntry.cpp:13
#, kde-format
msgctxt ""
"@label %1 is the distro name, %2 is the version, %3 is the 'build' which "
"should be a number, or 'rolling'"
msgid "%1 %2 Build: %3"
msgstr ""

#: package/contents/ui/main.qml:91
#, kde-format
msgctxt "@title"
msgid "Serial Number"
msgstr ""

#: package/contents/ui/main.qml:97 package/contents/ui/main.qml:177
#, kde-format
msgctxt "@action:button"
msgid "Copy to Clipboard"
msgstr ""

#: package/contents/ui/main.qml:123
#, kde-format
msgctxt "@action:button show a hidden entry in an overlay"
msgid "Show"
msgstr ""

#: package/contents/ui/main.qml:135
#, fuzzy, kde-format
#| msgid "Software"
msgctxt "@title:group"
msgid "Software"
msgstr "Software"

#: package/contents/ui/main.qml:145
#, fuzzy, kde-format
#| msgid "Hardware"
msgctxt "@title:group"
msgid "Hardware"
msgstr "Hardware"

#: package/contents/ui/main.qml:165
#, kde-format
msgctxt "@action:button launches kinfocenter from systemsettings"
msgid "Show More Information"
msgstr ""

#: package/contents/ui/main.qml:184
#, kde-format
msgctxt "@action:button"
msgid "Copy to Clipboard in English"
msgstr ""

#: PlasmaEntry.cpp:15
#, fuzzy, kde-format
msgid "KDE Plasma Version:"
msgstr "KDELibs Verzija:"

#~ msgid "OS Type:"
#~ msgstr "OS Tip:"

#, fuzzy
#~ msgctxt "@title"
#~ msgid "About Distribution"
#~ msgstr "O distribuciji"

#~ msgctxt "@label %1 is the CPU bit width (e.g. 32 or 64)"
#~ msgid "<numid>%1</numid>-bit"
#~ msgstr "<numid>%1</numid>-bit"
