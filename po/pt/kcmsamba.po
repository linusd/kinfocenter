msgid ""
msgstr ""
"Project-Id-Version: kcmsamba\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-17 00:29+0000\n"
"PO-Revision-Date: 2020-09-29 10:30+0100\n"
"Last-Translator: Pedro Morais <morais@kde.org>\n"
"Language-Team: pt <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-IgnoreConsistency: Open Files\n"
"X-Spell-Extra: GID UID KDE NFS PID SMB NetBIOS PATH localhost\n"
"X-Spell-Extra: Session Message Block usb sbin smbstatus smb conf\n"
"X-POFile-IgnoreConsistency: Hits\n"
"X-POFile-SpellExtra: UID kcmsamba showmount GID smbstatus conf smb Greven\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Alexander sbin Faure Network Matthias Glauche System\n"
"X-POFile-SpellExtra: Hoelzer Session Harald localhost Koschinski Message\n"
"X-POFile-SpellExtra: Wilco LanManager Neundorf Block usr drives Sitter\n"
"X-POFile-IgnoreConsistency: Type\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "José Nuno Pires"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zepires@gmail.com"

#: main.cpp:25
#, kde-format
msgid "kcmsamba"
msgstr "kcmsamba"

#: main.cpp:26
#, kde-format
msgid "Samba Status"
msgstr "Estado do Samba"

#: main.cpp:30
#, kde-format
msgid "(c) 2002-2020 KDE Information Control Module Samba Team"
msgstr "(c) 2002-2020 A Equipa de Samba do Módulo de Informação do KDE"

#: main.cpp:31
#, kde-format
msgid "Michael Glauche"
msgstr "Michael Glauche"

#: main.cpp:32
#, kde-format
msgid "Matthias Hoelzer"
msgstr "Matthias Hoelzer"

#: main.cpp:33
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: main.cpp:34
#, kde-format
msgid "Harald Koschinski"
msgstr "Harald Koschinski"

#: main.cpp:35
#, kde-format
msgid "Wilco Greven"
msgstr "Wilco Greven"

#: main.cpp:36
#, kde-format
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: main.cpp:37
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:17
#, kde-format
msgctxt "@title heading above listview"
msgid "User-Created Shares"
msgstr "Partilhas Criadas pelo Utilizador"

#: package/contents/ui/main.qml:22
#, kde-format
msgctxt "@title heading above listview"
msgid "Mounted Remote Shares"
msgstr "Partilhas Remotas Montadas"

#: package/contents/ui/main.qml:49
#, kde-format
msgctxt "@info place holder for empty listview"
msgid "There are no directories shared by users"
msgstr "Não há pastas partilhadas pelos utilizadores"

#: package/contents/ui/main.qml:89
#, kde-format
msgctxt "@info place holder for empty listview"
msgid "There are no Samba shares mounted on this system"
msgstr "Não há partilhas de Samba montadas neste sistema"

#: package/contents/ui/ShareListItem.qml:71
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid "Open folder properties to change share settings"
msgstr "Abrir as propriedades da pasta para mudar a configuração da partilha"
