# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2010, 2011, 2013, 2014, 2020.
# Tomáš Chvátal <tomas.chvatal@gmail.com>, 2013.
# Vit Pelcak <vpelcak@suse.cz>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmsamba\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-17 00:29+0000\n"
"PO-Revision-Date: 2021-06-03 12:04+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.04.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Lukáš Tinkl"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lukas@kde.org"

#: main.cpp:25
#, kde-format
msgid "kcmsamba"
msgstr "kcmsamba"

#: main.cpp:26
#, kde-format
msgid "Samba Status"
msgstr "Stav Samby"

#: main.cpp:30
#, kde-format
msgid "(c) 2002-2020 KDE Information Control Module Samba Team"
msgstr "(c) 2002-2020 KDE Tým Information Control Module Samba"

#: main.cpp:31
#, kde-format
msgid "Michael Glauche"
msgstr "Michael Glauche"

#: main.cpp:32
#, kde-format
msgid "Matthias Hoelzer"
msgstr "Matthias Hoelzer"

#: main.cpp:33
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: main.cpp:34
#, kde-format
msgid "Harald Koschinski"
msgstr "Harald Koschinski"

#: main.cpp:35
#, kde-format
msgid "Wilco Greven"
msgstr "Wilco Greven"

#: main.cpp:36
#, kde-format
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: main.cpp:37
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:17
#, kde-format
msgctxt "@title heading above listview"
msgid "User-Created Shares"
msgstr ""

#: package/contents/ui/main.qml:22
#, kde-format
msgctxt "@title heading above listview"
msgid "Mounted Remote Shares"
msgstr ""

#: package/contents/ui/main.qml:49
#, kde-format
msgctxt "@info place holder for empty listview"
msgid "There are no directories shared by users"
msgstr ""

#: package/contents/ui/main.qml:89
#, kde-format
msgctxt "@info place holder for empty listview"
msgid "There are no Samba shares mounted on this system"
msgstr ""

#: package/contents/ui/ShareListItem.qml:71
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid "Open folder properties to change share settings"
msgstr ""
