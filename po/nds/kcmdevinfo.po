# translation of kcm_devinfo.po to Low Saxon
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Manfred Wiese <m.j.wiese@web.de>, 2010, 2011.
# Sönke Dibbern <s_dibbern@web.de>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: kcm_devinfo\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-16 00:48+0000\n"
"PO-Revision-Date: 2014-02-16 23:06+0100\n"
"Last-Translator: Sönke Dibbern <s_dibbern@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Manfred Wiese, Sönke Dibbern"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "m.j.wiese@web.de, s_dibbern@web.de"

#: devicelisting.cpp:41
#, kde-format
msgctxt "Device Listing Whats This"
msgid "Shows all the devices that are currently listed."
msgstr "Wiest all opstunns oplist Reedschappen."

#: devicelisting.cpp:44
#, kde-format
msgid "Devices"
msgstr "Reedschappen"

#: devicelisting.cpp:57
#, kde-format
msgid "Collapse All"
msgstr "All infoolden"

#: devicelisting.cpp:60
#, kde-format
msgid "Expand All"
msgstr "All utfoolden"

#: devicelisting.cpp:63
#, kde-format
msgid "Show All Devices"
msgstr "All Reedschappen wiesen"

#: devicelisting.cpp:66
#, kde-format
msgid "Show Relevant Devices"
msgstr "Wichtige Reedschappen wiesen"

#: devicelisting.cpp:95
#, kde-format
msgctxt "unknown device type"
msgid "Unknown"
msgstr "Nich begäng"

#: devicelisting.cpp:136 devinfo.cpp:79
#, kde-format
msgctxt "no device UDI"
msgid "None"
msgstr "Keen"

#: devinfo.cpp:28
#, kde-format
msgid "kcmdevinfo"
msgstr "kcmdevinfo"

#: devinfo.cpp:28
#, fuzzy, kde-format
#| msgid "KDE Solid Based Device Viewer"
msgid "Device Viewer"
msgstr "Op »Solid« opbuut Reedschappenkieker för KDE"

#: devinfo.cpp:28
#, kde-format
msgid "(c) 2010 David Hubner"
msgstr "© 2010 David Hubner"

#: devinfo.cpp:58
#, kde-format
msgid "UDI: "
msgstr "UDI: "

#: devinfo.cpp:66
#, kde-format
msgctxt "Udi Whats This"
msgid "Shows the current device's UDI (Unique Device Identifier)"
msgstr ""
"Wiest de aktuell Reedschap ehr eenkennig Beteker (UDI = Unique Device "
"Identifier)"

#: infopanel.cpp:20
#, kde-format
msgid "Device Information"
msgstr "Reedschap-Informatschonen"

#: infopanel.cpp:29
#, kde-format
msgctxt "Info Panel Whats This"
msgid "Shows information about the currently selected device."
msgstr "Wiest Informatschonen över de aktuell utsöcht Reedschap."

#: infopanel.cpp:56
#, kde-format
msgid ""
"\n"
"Solid Based Device Viewer Module"
msgstr ""
"\n"
"Op »Solid« opbuut Reedschappenkieker för KDE"

#: infopanel.cpp:119
#, kde-format
msgid "Description: "
msgstr ""

#: infopanel.cpp:121
#, kde-format
msgid "Product: "
msgstr "Produkt: "

#: infopanel.cpp:123
#, kde-format
msgid "Vendor: "
msgstr "Leverant: "

#: infopanel.cpp:145
#, kde-format
msgid "Yes"
msgstr "Jo"

#: infopanel.cpp:147
#, kde-format
msgid "No"
msgstr "Nee"

#: infopanel.h:36
#, kde-format
msgctxt "name of something is not known"
msgid "Unknown"
msgstr "Nich begäng"

#: soldevice.cpp:65
#, kde-format
msgctxt "unknown device"
msgid "Unknown"
msgstr "Nich begäng"

#: soldevice.cpp:91
#, kde-format
msgctxt "Default device tooltip"
msgid "A Device"
msgstr "En Reedschapp"

#: soldevicetypes.cpp:43
#, kde-format
msgid "Processors"
msgstr "Perzessern"

#: soldevicetypes.cpp:59
#, kde-format
msgid "Processor %1"
msgstr "Perzesser %1"

#: soldevicetypes.cpp:76
#, kde-format
msgid "Intel MMX"
msgstr "Intel MMX"

#: soldevicetypes.cpp:79
#, kde-format
msgid "Intel SSE"
msgstr "Intel SSE"

#: soldevicetypes.cpp:82
#, kde-format
msgid "Intel SSE2"
msgstr "Intel SSE2"

#: soldevicetypes.cpp:85
#, kde-format
msgid "Intel SSE3"
msgstr "Intel SSE3"

#: soldevicetypes.cpp:88
#, fuzzy, kde-format
#| msgid "Intel SSE3"
msgid "Intel SSSE3"
msgstr "Intel SSE3"

#: soldevicetypes.cpp:91
#, fuzzy, kde-format
#| msgid "Intel SSE4"
msgid "Intel SSE4.1"
msgstr "Intel SSE4"

#: soldevicetypes.cpp:94
#, fuzzy, kde-format
#| msgid "Intel SSE4"
msgid "Intel SSE4.2"
msgstr "Intel SSE4"

#: soldevicetypes.cpp:97
#, fuzzy, kde-format
#| msgid "AMD 3DNow"
msgid "AMD 3DNow!"
msgstr "AMD 3DNow"

#: soldevicetypes.cpp:100
#, kde-format
msgid "ATI IVEC"
msgstr "ATI IVEC"

#: soldevicetypes.cpp:103
#, kde-format
msgctxt "no instruction set extensions"
msgid "None"
msgstr "Keen"

#: soldevicetypes.cpp:106
#, kde-format
msgid "Processor Number: "
msgstr "Perzessernummer: "

#: soldevicetypes.cpp:106
#, kde-format
msgid "Max Speed: "
msgstr "Hööchst Gauheit: "

#: soldevicetypes.cpp:107
#, kde-format
msgid "Supported Instruction Sets: "
msgstr "Ünnerstütt Anwiesen-Setten: "

#: soldevicetypes.cpp:132
#, kde-format
msgid "Storage Drives"
msgstr "Spiekerreedschappen"

#: soldevicetypes.cpp:151
#, kde-format
msgid "Hard Disk Drive"
msgstr "Fastplaat"

#: soldevicetypes.cpp:154
#, kde-format
msgid "Compact Flash Reader"
msgstr "Leser för Spiekerkoorten (Compact Flash)"

#: soldevicetypes.cpp:157
#, kde-format
msgid "Smart Media Reader"
msgstr "Leser för Smartkoorten"

#: soldevicetypes.cpp:160
#, kde-format
msgid "SD/MMC Reader"
msgstr "Leser för SD-/MMC-Koorten"

#: soldevicetypes.cpp:163
#, kde-format
msgid "Optical Drive"
msgstr "Optsch Loopwark"

#: soldevicetypes.cpp:166
#, kde-format
msgid "Memory Stick Reader"
msgstr "Leser för Spiekerstiften"

#: soldevicetypes.cpp:169
#, kde-format
msgid "xD Reader"
msgstr "Leser för xD-Koorten"

#: soldevicetypes.cpp:172
#, kde-format
msgid "Unknown Drive"
msgstr "Nich begäng Loopwark"

#: soldevicetypes.cpp:192
#, kde-format
msgid "IDE"
msgstr "IDE"

#: soldevicetypes.cpp:195
#, kde-format
msgid "USB"
msgstr "USB"

#: soldevicetypes.cpp:198
#, kde-format
msgid "IEEE1394"
msgstr "IEEE 1394"

#: soldevicetypes.cpp:201
#, kde-format
msgid "SCSI"
msgstr "SCSI"

#: soldevicetypes.cpp:204
#, kde-format
msgid "SATA"
msgstr "SATA"

#: soldevicetypes.cpp:207
#, kde-format
msgctxt "platform storage bus"
msgid "Platform"
msgstr "Systeem"

#: soldevicetypes.cpp:210
#, kde-format
msgctxt "unknown storage bus"
msgid "Unknown"
msgstr "Nich begäng"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Bus: "
msgstr "Bus: "

#: soldevicetypes.cpp:213
#, kde-format
msgid "Hotpluggable?"
msgstr "Jümmers tokoppelbor?"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Removable?"
msgstr "Tuuschbor?"

#: soldevicetypes.cpp:257
#, kde-format
msgid "Unused"
msgstr "Nich bruukt"

#: soldevicetypes.cpp:260
#, kde-format
msgid "File System"
msgstr "Dateisysteem"

#: soldevicetypes.cpp:263
#, kde-format
msgid "Partition Table"
msgstr "Partitschonentabell"

#: soldevicetypes.cpp:266
#, kde-format
msgid "Raid"
msgstr "Raid"

#: soldevicetypes.cpp:269
#, kde-format
msgid "Encrypted"
msgstr "Verslötelt"

#: soldevicetypes.cpp:272
#, kde-format
msgctxt "unknown volume usage"
msgid "Unknown"
msgstr "Nich begäng"

#: soldevicetypes.cpp:275
#, kde-format
msgid "File System Type: "
msgstr "Dateisysteemtyp: "

#: soldevicetypes.cpp:275
#, kde-format
msgid "Label: "
msgstr "Naam: "

#: soldevicetypes.cpp:276
#, kde-format
msgid "Not Set"
msgstr "Nich fastleggt"

#: soldevicetypes.cpp:276
#, kde-format
msgid "Volume Usage: "
msgstr "Spiekerreedschap-Bruuk: "

#: soldevicetypes.cpp:276
#, kde-format
msgid "UUID: "
msgstr "UUID: "

#: soldevicetypes.cpp:282
#, kde-format
msgid "Mounted At: "
msgstr "Inhangt in: "

#: soldevicetypes.cpp:282
#, kde-format
msgid "Not Mounted"
msgstr "Nich inhangt"

#: soldevicetypes.cpp:287
#, kde-format
msgid "Volume Space:"
msgstr "Spiekerreedschap-Grött:"

#: soldevicetypes.cpp:297
#, kde-format
msgctxt "Available space out of total partition size (percent used)"
msgid "%1 free of %2 (%3% used)"
msgstr "%1 free vun %2 (%3% bruukt)"

#: soldevicetypes.cpp:303
#, kde-format
msgid "No data available"
msgstr "Keen Daten verföögbor"

#: soldevicetypes.cpp:330
#, kde-format
msgid "Multimedia Players"
msgstr "Multimedia--Afspelers"

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Drivers: "
msgstr "Ünnerstütt Drievers: "

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Protocols: "
msgstr "Ünnerstütt Protokollen: "

#: soldevicetypes.cpp:369
#, kde-format
msgid "Cameras"
msgstr "Kameras"

#: soldevicetypes.cpp:408
#, kde-format
msgid "Batteries"
msgstr "Batterien"

#: soldevicetypes.cpp:430
#, kde-format
msgid "PDA"
msgstr "PDA"

#: soldevicetypes.cpp:433
#, kde-format
msgid "UPS"
msgstr "Nootstroomreedschap"

#: soldevicetypes.cpp:436
#, kde-format
msgid "Primary"
msgstr "Hööft"

#: soldevicetypes.cpp:439
#, kde-format
msgid "Mouse"
msgstr "Muus"

#: soldevicetypes.cpp:442
#, kde-format
msgid "Keyboard"
msgstr "Tastatuur"

#: soldevicetypes.cpp:445
#, kde-format
msgid "Keyboard + Mouse"
msgstr "Tastatuur un Muus"

#: soldevicetypes.cpp:448
#, kde-format
msgid "Camera"
msgstr "Kamera"

#: soldevicetypes.cpp:451
#, kde-format
msgid "Phone"
msgstr ""

#: soldevicetypes.cpp:454
#, kde-format
msgctxt "Screen"
msgid "Monitor"
msgstr ""

#: soldevicetypes.cpp:457
#, kde-format
msgctxt "Wireless game pad or joystick battery"
msgid "Gaming Input"
msgstr ""

#: soldevicetypes.cpp:460
#, kde-format
msgctxt "unknown battery type"
msgid "Unknown"
msgstr "Nich begäng"

#: soldevicetypes.cpp:466
#, kde-format
msgid "Charging"
msgstr "Bi to laden"

#: soldevicetypes.cpp:469
#, kde-format
msgid "Discharging"
msgstr "Bi to utladen"

#: soldevicetypes.cpp:472
#, fuzzy, kde-format
#| msgid "No Charge"
msgid "Fully Charged"
msgstr "Oplaadtostand"

#: soldevicetypes.cpp:475
#, kde-format
msgid "No Charge"
msgstr "Oplaadtostand"

#: soldevicetypes.cpp:478
#, kde-format
msgid "Battery Type: "
msgstr "Batterietyp: "

#: soldevicetypes.cpp:478
#, kde-format
msgid "Charge Status: "
msgstr "Oplaadtostand: "

#: soldevicetypes.cpp:478
#, fuzzy, kde-format
#| msgid "Charge Status: "
msgid "Charge Percent: "
msgstr "Oplaadtostand: "

#~ msgid "Network Interfaces"
#~ msgstr "Nettwark-Koppelsteden"

#~ msgid "Connected"
#~ msgstr "Tokoppelt"

#~ msgid "Wireless"
#~ msgstr "Funk"

#~ msgid "Wired"
#~ msgstr "Kavel"

#~ msgid "Hardware Address: "
#~ msgstr "Reedschap-Adress: "

#~ msgid "Wireless?"
#~ msgstr "Funk?"

#~ msgid "Audio Interfaces"
#~ msgstr "Klang-Koppelsteden"

#~ msgid "Alsa Interfaces"
#~ msgstr "Alsa-Koppelsteden"

#~ msgid "Open Sound System Interfaces"
#~ msgstr "OSS-Koppelsteden"

#~ msgid "Control"
#~ msgstr "Stüern"

#~ msgid "Input"
#~ msgstr "Ingaav"

#~ msgid "Output"
#~ msgstr "Utgaav"

#~ msgctxt "unknown audio interface type"
#~ msgid "Unknown"
#~ msgstr "Nich begäng"

#~ msgid "Internal Soundcard"
#~ msgstr "Inbuut Klangkoort"

#~ msgid "USB Soundcard"
#~ msgstr "USB-Klangkoort"

#~ msgid "Firewire Soundcard"
#~ msgstr "FireWire-Klangkoort"

#~ msgid "Headset"
#~ msgstr "Snacksett"

#~ msgid "Modem"
#~ msgstr "Modem"

#~ msgctxt "unknown sound card type"
#~ msgid "Unknown"
#~ msgstr "Nich begäng"

#~ msgid "Audio Interface Type: "
#~ msgstr "Klangkoppelsteed-Typ: "

#~ msgid "Soundcard Type: "
#~ msgstr "Klangkoort-Typ: "

#~ msgid "Device Buttons"
#~ msgstr "Reedschapknööp"

#~ msgid "Lid Button"
#~ msgstr "Deckelknoop"

#~ msgid "Power Button"
#~ msgstr "Anmaakknoop"

#~ msgid "Sleep Button"
#~ msgstr "Slaapknoop"

#~ msgid "Tablet Button"
#~ msgstr "Tablett-Knoop"

#~ msgid "Unknown Button"
#~ msgstr "Nich begäng Knoop"

#~ msgid "Button type: "
#~ msgstr "Knooptyp: "

#~ msgid "Has State?"
#~ msgstr "Hett Status?"

#~ msgid "AC Adapters"
#~ msgstr "Stroomnett"

#~ msgid "Is plugged in?"
#~ msgstr "Tokoppelt?"

#~ msgid "Digital Video Broadcasting Devices"
#~ msgstr "DVB-Reedschappen"

#~ msgid "Audio"
#~ msgstr "Klang"

#~ msgid "Conditional access system"
#~ msgstr "Verslötelt Systemen"

#~ msgid "Demux"
#~ msgstr "Demux"

#~ msgid "Digital video recorder"
#~ msgstr "Digitaal Video-Opnehmer"

#~ msgid "Front end"
#~ msgstr "Böversiet"

#~ msgid "Network"
#~ msgstr "Nettwark"

#~ msgid "On-Screen display"
#~ msgstr "Op-Schirm-Dorstellen"

#~ msgid "Security and content protection"
#~ msgstr "Sekerheit un Inholtschuul"

#~ msgid "Video"
#~ msgstr "Video"

#~ msgid "Device Type: "
#~ msgstr "Reedschaptyp: "

#~ msgid "Serial Devices"
#~ msgstr "Seriell Reedschappen"

#~ msgctxt "platform serial interface type"
#~ msgid "Platform"
#~ msgstr "Systeem"

#~ msgctxt "unknown serial interface type"
#~ msgid "Unknown"
#~ msgstr "Nich begäng"

#~ msgctxt "unknown port"
#~ msgid "Unknown"
#~ msgstr "Nich begäng"

#~ msgid "Serial Type: "
#~ msgstr "Seriell Typ: "

#~ msgid "Port: "
#~ msgstr "Port: "

#~ msgid "Smart Card Devices"
#~ msgstr "Smartkoorten"

#~ msgid "Card Reader"
#~ msgstr "Koort-Leser"

#~ msgid "Crypto Token"
#~ msgstr "Togangslötel"

#~ msgctxt "unknown smart card type"
#~ msgid "Unknown"
#~ msgstr "Nich begäng"

#~ msgid "Smart Card Type: "
#~ msgstr "Smartkoort-Typ: "

#~ msgid "Video Devices"
#~ msgstr "Videoreedschappen"

#~ msgid "Device unable to be cast to correct device"
#~ msgstr "De Reedschap lett sik keen gellen Reedschap towiesen."

#~ msgid "Percentage Used / Available: "
#~ msgstr "Perzent bruukt/verföögbor"

#~ msgid "Not Connected"
#~ msgstr "Nich tokoppelt"

#~ msgid "IP Address (V4): "
#~ msgstr "IP-Address (V4): "

#~ msgid "Gateway (V4): "
#~ msgstr "Döörreekner (V4): "
